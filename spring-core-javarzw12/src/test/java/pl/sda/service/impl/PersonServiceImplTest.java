package pl.sda.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.sda.model.Person;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonServiceImplTest {

    private final ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");

    @Test
    public void shouldGetAllPersons() {
        //given
        PersonServiceImpl underTest = ac.getBean("personServiceImpl", PersonServiceImpl.class);

        //when
        List<Person> result = underTest.getAll();

        //then
        Assertions.assertNotNull(result);
        Assertions.assertEquals(3, result.size());
    }

}