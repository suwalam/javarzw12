package pl.sda.service;

public interface ViewCounterService {

    void increment();

    Integer getViewCounter();

}
