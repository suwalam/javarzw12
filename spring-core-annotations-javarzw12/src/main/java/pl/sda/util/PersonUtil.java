package pl.sda.util;

public class PersonUtil {

    public int parseId(String id) {

        if (id == null || id.trim().isEmpty()) {
            throw new IllegalArgumentException("Id is empty " + id);
        }

        return Integer.parseInt(id);
    }

}
