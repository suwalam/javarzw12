package pl.sda.model;

public enum RoleType {

    USER("USER"), ADMIN("ADMIN");

    private String roleName;

    RoleType(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getAuthority() {
        return "ROLE_".concat(roleName);
    }
}
