package pl.sda.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.exception.ClientAccountNotFoundException;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.model.ClientAccount;
import pl.sda.repository.ClientAccountRepository;


import java.math.BigDecimal;

@Transactional
@Service
public class ClientAccountServiceImpl implements ClientAccountService {

    private final ClientAccountRepository accountRepository;

    private final HistoryAccountService historyAccountService;

    public ClientAccountServiceImpl(ClientAccountRepository accountRepository, HistoryAccountService historyAccountService) {
        this.accountRepository = accountRepository;
        this.historyAccountService = historyAccountService;
    }

    @Override
    public ClientAccount getById(Integer id) {
        return accountRepository.getById(id);
    }

    @Override
    public ClientAccount getByNumber(String number) throws ClientAccountNotFoundException {
        ClientAccount account = accountRepository.findByNumber(number);

        if (account == null) {
            throw new ClientAccountNotFoundException("Nie znaleziono konta o numerze " + number);
        }

        return account;
    }

    @Override
    public void transfer(Integer srcId, String destNumber, BigDecimal amount) throws ClientAccountNotFoundException, InsufficientBalanceException {

        ClientAccount srcAccount = accountRepository.getById(srcId);

        if (srcAccount.getBalance().compareTo(amount) < 0) {
            throw new InsufficientBalanceException("Za mało środków na koncie do wykonania przelewu na kwotę: " + amount);
        }

        ClientAccount destAccount = getByNumber(destNumber);

        srcAccount.setBalance(srcAccount.getBalance().subtract(amount));
        destAccount.setBalance(destAccount.getBalance().add(amount));

        accountRepository.save(srcAccount);
        accountRepository.save(destAccount);

        //zapis historii dla konta źródłowego
        historyAccountService.save(
                destAccount.getClient().getName(),
                destAccount.getClient().getSurname(),
                srcAccount,
                amount.negate(),
                destNumber);

        //zapis historii dla konta docelowego
        historyAccountService.save(
                srcAccount.getClient().getName(),
                srcAccount.getClient().getSurname(),
                destAccount,
                amount,
                srcAccount.getNumber());
    }
}
