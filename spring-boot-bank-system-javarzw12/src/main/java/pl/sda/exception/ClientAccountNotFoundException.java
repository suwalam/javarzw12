package pl.sda.exception;

public class ClientAccountNotFoundException extends Exception {

    public ClientAccountNotFoundException(String message) {
        super(message);
    }
}
