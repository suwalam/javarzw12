package pl.sda.service;

import pl.sda.model.ClientAccount;

import java.math.BigDecimal;

public interface HistoryAccountService {

    void save(String name, String surname, ClientAccount account, BigDecimal amount, String number);

}
