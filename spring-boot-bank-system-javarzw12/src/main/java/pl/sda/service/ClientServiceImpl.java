package pl.sda.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Client;
import pl.sda.model.ClientAccount;
import pl.sda.repository.ClientAccountRepository;
import pl.sda.repository.ClientRepository;
import pl.sda.service.ClientService;
import pl.sda.util.ClientAccountFactory;

import java.util.List;

@Slf4j
@Transactional
@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    private final ClientAccountFactory accountFactory;

    private final ClientAccountRepository accountRepository;

    public ClientServiceImpl(ClientRepository clientRepository, ClientAccountFactory accountFactory, ClientAccountRepository accountRepository) {
        this.clientRepository = clientRepository;
        this.accountFactory = accountFactory;
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    @Override
    public void addAccount(Integer id) {
        Client client = clientRepository.getById(id);
        ClientAccount newAccount = accountFactory.createClientAccount(client);
        client.getAccounts().add(newAccount);
        accountRepository.save(newAccount);
    }

    @Override
    public void delete(Integer id) {
        clientRepository.deleteById(id);
        log.info("Deleted client with id " + id);
    }

    @Override
    public Client getById(Integer id) {
        return clientRepository.getById(id);
    }

    @Override
    public void update(Client client) {
        clientRepository.save(client);
    }
}
