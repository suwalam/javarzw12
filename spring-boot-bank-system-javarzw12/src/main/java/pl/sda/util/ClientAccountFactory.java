package pl.sda.util;

import org.springframework.stereotype.Component;
import pl.sda.model.Client;
import pl.sda.model.ClientAccount;

import java.math.BigDecimal;

@Component
public class ClientAccountFactory {

    public ClientAccount createClientAccount(Client client) {
        ClientAccount clientAccount = new ClientAccount();
        clientAccount.setClient(client);
        clientAccount.setBalance(BigDecimal.ZERO);
        clientAccount.setNumber(generateAccountNumber());
        return clientAccount;
    }

    private String generateAccountNumber() {
        return "" + System.nanoTime();
    }
}
