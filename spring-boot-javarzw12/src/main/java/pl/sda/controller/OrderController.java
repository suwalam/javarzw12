package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Order;
import pl.sda.service.OrderService;

import javax.validation.Valid;

@Slf4j
@RequestMapping("/order")
@Controller
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/create")
    public String showOrderForm(Model model) {
        model.addAttribute("emptyOrder", new Order());

        return "order";
    }

    @GetMapping("/all")
    public String showAllOrders(Model model) {
        model.addAttribute("orders", orderService.getAll());

        return "order-list";
    }

    @PostMapping("/save")
    public String handleNewOrder(@Valid @ModelAttribute("emptyOrder") Order order, Errors errors) {

        log.info("Handle new order: " + order);

        if (errors.hasErrors()) {
            log.error("Error occured in frontend: " + errors.getFieldErrors());
            return "order";
        }

        orderService.save(order);

        log.info("All orders: " + orderService.getAll());

        return "redirect:/order/all";

    }

    @GetMapping("/edit/{id}")
    public String editOrder(@PathVariable Integer id, Model model) {
        model.addAttribute("order", orderService.getById(id));
        return "order-edit";
    }

    @PostMapping("/update")
    public String updateOrder(@ModelAttribute Order order) {
        log.info("Handle order to update: " + order);
        orderService.update(order);

        return "redirect:/order/all";
    }

    @GetMapping("/delete/{id}")
    public String deleteOrder(@PathVariable Integer id) {
        orderService.delete(id);
        log.info("Deleted order with id: " + id);
        return "redirect:/order/all";
    }
}
