package pl.sda.service;

import org.springframework.stereotype.Service;
import pl.sda.model.ViewCounter;
import pl.sda.repository.ViewCounterRepository;

@Service
public class ViewCounterServiceImpl implements ViewCounterService {

    private final ViewCounterRepository counterRepository;

    private static final Integer ID = 1;

    public ViewCounterServiceImpl(ViewCounterRepository counterRepository) {
        this.counterRepository = counterRepository;
    }

    @Override
    public void increment() {
        final ViewCounter viewCounter = counterRepository.getById(ID);
        viewCounter.setCounter(viewCounter.getCounter() + 1);
        counterRepository.save(viewCounter);
    }

    @Override
    public Integer getViewCounter() {
        return counterRepository.getById(ID).getCounter();
    }
}
