package pl.sda.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Client;
import pl.sda.model.User;
import pl.sda.service.ClientService;
import pl.sda.service.UserService;
import pl.sda.service.ViewCounterService;

@RequestMapping("/admin")
@Controller
public class AdminController {

    private final ClientService clientService;

    private final UserService userService;

    private final ViewCounterService viewCounterService;

    public AdminController(ClientService clientService, UserService userService, ViewCounterService viewCounterService) {
        this.clientService = clientService;
        this.userService = userService;
        this.viewCounterService = viewCounterService;
    }

    @GetMapping("/main")
    public String adminMain(Model model) {
        viewCounterService.increment();
        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("username", username);
        model.addAttribute("counter", viewCounterService.getViewCounter().toString());
        return "admin-main";
    }

    @GetMapping("/deleteClient/{clientId}")
    public String deleteClient(@PathVariable Integer clientId) {
        //Usuwamy USERa, a nie klienta
        userService.deleteByClientId(clientId);

        return "redirect:/admin/main";

    }

    @GetMapping("/addAccount/{clientId}")
    public String addAccountToClient(@PathVariable Integer clientId) {
        clientService.addAccount(clientId);

        return "redirect:/admin/main";
    }

    @GetMapping("/editClient/{clientId}")
    public String editClient(@PathVariable Integer clientId, Model model) {
        final Client client = clientService.getById(clientId);
        model.addAttribute("client", client);
        return "client-edit";
    }

    @PostMapping("/client/update")
    public String updateClient(@ModelAttribute Client client) {
        clientService.update(client);

        return "redirect:/admin/main";
    }

    @GetMapping("/createAdmin")
    public String createAdmin(Model model) {
        model.addAttribute("user", new User());
        return "admin-create-admin";
    }

    @PostMapping("/createAdmin")
    public String saveAdmin(@ModelAttribute User user) {
        userService.saveAdmin(user);

        return "redirect:/admin/main";
    }

}
