package pl.sda.service;

import pl.sda.model.User;

public interface UserService {

    void deleteByClientId(Integer id);

    User findByUsername(String username);

    void saveUser(User user);

    void saveAdmin(User user);
}
