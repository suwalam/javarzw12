package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBankSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBankSystemApplication.class, args);
	}

}
