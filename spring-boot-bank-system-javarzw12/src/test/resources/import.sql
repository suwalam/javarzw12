insert into client(id, name, surname, address, pesel) values (1, 'Jan', 'Kowalski', 'Warszawa', '12345678910');
insert into client(id, name, surname, address, pesel) values (2, 'Michał', 'Nowak', 'Warszawa', '12345678911');

insert into client_account(id, number, balance, client_id) values(1, '12345', 120.0,  1);
insert into client_account(id, number, balance, client_id) values(2, '54321', 1200.0,  2);

