insert into role (id, name) values(1, 'ADMIN');
insert into role (id, name) values(2, 'USER');

insert into client(id, name, surname, address, pesel) values (1, 'Jan', 'Kowalski', 'Warszawa', '12345678910');
insert into client_account(id, number, balance, client_id) values(1, '12345', 120.0,  1);
insert into history_account(id, amount, name, number, surname, transfer_date, account_id) values(1, 820, 'Michal', '54321', 'Nowak', '2020-10-10 22:43:54', 1);

insert into user (id, username, password) values(1, 'admin', '$2a$10$csIVj5k8CB74npTTstUQuO3ObjmaM6y8cRMxvsKqpHmT3zNQxY/IK');
insert into user (id, username, password, client_id) values(2, 'jan.kowalski', '$2a$10$m7BBzoGFKzb8e3/q5AK/SuC.qNKnyh.GcHkKnJYXsvJGQREhZe0Zu', 1);

insert into user_role (user_id, role_id) values(1,1);
insert into user_role (user_id, role_id) values(2,2);

insert into view_counter (id, counter) values(1,0);


