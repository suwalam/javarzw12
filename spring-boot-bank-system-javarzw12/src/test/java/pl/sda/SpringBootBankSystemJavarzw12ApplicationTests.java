package pl.sda;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.exception.ClientAccountNotFoundException;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.model.ClientAccount;
import pl.sda.service.ClientAccountService;

import java.math.BigDecimal;

@SpringBootTest
class SpringBootBankSystemJavarzw12ApplicationTests {

	@Autowired
	private ClientAccountService accountService;

	@Test
	void contextLoads() throws ClientAccountNotFoundException {
		ClientAccount clientAccount = accountService.getByNumber("12345");
		System.out.println(clientAccount);
	}
}
