package pl.sda.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sda.dao.PersonDao;
import pl.sda.dao.impl.PersonDaoMemory;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;
import pl.sda.util.PersonUtil;

@Configuration
public class PersonConfiguration {

    @Bean
    public PersonDao personDaoMemory() {
        return new PersonDaoMemory();
    }

    @Bean
    public PersonUtil personUtil() {
        return new PersonUtil();
    }

    @Bean
    public PersonService personServiceImpl() {
        PersonServiceImpl personServiceImpl = new PersonServiceImpl(personDaoMemory());
        personServiceImpl.setPersonUtil(personUtil());
        return  personServiceImpl;
    }

}
