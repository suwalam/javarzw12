package pl.sda.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.exception.ClientAccountNotFoundException;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.model.ClientAccount;

import java.math.BigDecimal;

@SpringBootTest
class ClientAccountServiceImplTest {

    @Autowired
    private ClientAccountService accountService;

    @Transactional
    @Test
    public void shouldTransferAmount() throws ClientAccountNotFoundException, InsufficientBalanceException {
        //given
        Integer srcId = 1;
        String destNumber = "54321";
        BigDecimal amount = new BigDecimal(100.00);
        BigDecimal srcAmount = new BigDecimal(120.00);
        BigDecimal destAmount = new BigDecimal(1200.00);

        //when
        accountService.transfer(srcId, destNumber, amount);

        //then
        ClientAccount srcAccount = accountService.getById(srcId);
        ClientAccount destAccount = accountService.getByNumber(destNumber);

        Assertions.assertEquals(srcAmount.subtract(amount).doubleValue(), srcAccount.getBalance().doubleValue());
        Assertions.assertEquals(destAmount.add(amount).doubleValue(), destAccount.getBalance().doubleValue());
    }

}