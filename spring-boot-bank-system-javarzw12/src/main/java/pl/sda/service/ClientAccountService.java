package pl.sda.service;

import pl.sda.exception.ClientAccountNotFoundException;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.model.ClientAccount;

import java.math.BigDecimal;

public interface ClientAccountService {

    ClientAccount getById(Integer id);

    ClientAccount getByNumber(String number) throws ClientAccountNotFoundException;

    void transfer(Integer srcId, String destNumber, BigDecimal amount)
            throws ClientAccountNotFoundException, InsufficientBalanceException;

}
