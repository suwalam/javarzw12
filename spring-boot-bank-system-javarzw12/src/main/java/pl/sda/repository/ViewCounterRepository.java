package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.model.ViewCounter;

@Repository
public interface ViewCounterRepository extends JpaRepository<ViewCounter, Integer> {

}
