package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Slf4j
@Controller
public class DefaultController {

    @GetMapping("/")
    public String defaultPath() {
      log.info("Return default.html view");

        return "default";
    }

    @GetMapping("/message")
    public String message(Model model) {
        model.addAttribute("msg", "Wiadomość z kontrolera DefaultController");

        model.addAttribute("elements", Arrays.asList("one", "two", "three"));

        return "msg-view";
    }

}
