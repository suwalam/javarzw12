package pl.sda.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.model.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {
}
