package pl.sda.service.impl;

import org.springframework.stereotype.Service;
import pl.sda.model.Order;
import pl.sda.repository.OrderRepository;
import pl.sda.service.OrderService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public void save(Order order) {
        order.setCreateDate(LocalDateTime.now());
        orderRepository.save(order);
    }

    @Override
    public List<Order> getAll() {

        final List<Order> result = new ArrayList<>();

        orderRepository.findAll().forEach(o -> result.add(o));

        return result;
    }

    @Override
    public void update(Order order) {
        orderRepository.save(order);
    }

    @Override
    public Order getById(Integer id) {
        return orderRepository
                .findById(id)
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void delete(Integer id) {
        orderRepository.deleteById(id);
    }
}
