package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByClientId(Integer id);

    User findByUsername(String username);

}
