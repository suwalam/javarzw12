package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.dto.TransferDto;
import pl.sda.exception.ClientAccountNotFoundException;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.model.Client;
import pl.sda.model.ClientAccount;
import pl.sda.model.User;
import pl.sda.service.ClientAccountService;
import pl.sda.service.ClientService;
import pl.sda.service.UserService;

@Slf4j
@RequestMapping("/client")
@Controller
public class ClientController {

    private final ClientService clientService;

    private final ClientAccountService accountService;

    private final UserService userService;

    public ClientController(ClientService clientService, ClientAccountService accountService, UserService userService) {
        this.clientService = clientService;
        this.accountService = accountService;
        this.userService = userService;
    }

    @GetMapping("/details")
    public String clientDetails(Model model) {

        final String username = SecurityContextHolder.getContext().getAuthentication().getName();

        final User user = userService.findByUsername(username);

        Client client = user.getClient();
        model.addAttribute("client", client);

        return "client-detail";
    }

    @GetMapping("/history/{accountId}")
    public String accountHistory(@PathVariable Integer accountId, Model model) {
        ClientAccount clientAccount = accountService.getById(accountId);
        model.addAttribute("histories", clientAccount.getHistoryAccounts());

        return "history";
    }

    @GetMapping("/transfer/{srcAccountId}")
    public String transfer(@PathVariable Integer srcAccountId, Model model) {
        TransferDto transferDto = new TransferDto();
        transferDto.setAccountId(srcAccountId);
        model.addAttribute("transferDto", transferDto);

        return "transfer";
    }

    @PostMapping("/transfer")
    public String handleTransfer(@ModelAttribute TransferDto transferDto, Model model) {

        log.info("Received transferDto: " + transferDto);

        try {
            accountService.transfer(transferDto.getAccountId(),
                    transferDto.getNumber(),
                    transferDto.getAmount());
        } catch (ClientAccountNotFoundException | InsufficientBalanceException e) {
            log.error("Błąd przy realizacji przelewu", e);
            model.addAttribute("errorMessage", e.getMessage());
            return "error";
        }

        log.info("Transfer successful");

        return "redirect:/client/details";

    }
}
