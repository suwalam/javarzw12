package pl.sda.service;

import pl.sda.model.Client;

import java.util.List;

public interface ClientService {

    List<Client> getAll();

    void addAccount(Integer id);

    void delete(Integer id);

    Client getById(Integer id);

    void update(Client client);

}
