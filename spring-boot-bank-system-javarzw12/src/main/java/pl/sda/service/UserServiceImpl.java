package pl.sda.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.model.*;
import pl.sda.repository.ClientAccountRepository;
import pl.sda.repository.ClientRepository;
import pl.sda.repository.RoleRepository;
import pl.sda.repository.UserRepository;
import pl.sda.util.ClientAccountFactory;

import java.util.Arrays;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final ClientAccountFactory clientAccountFactory;

    private final ClientRepository clientRepository;

    private final ClientAccountRepository clientAccountRepository;

    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, ClientAccountFactory clientAccountFactory, ClientRepository clientRepository, ClientAccountRepository clientAccountRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.clientAccountFactory = clientAccountFactory;
        this.clientRepository = clientRepository;
        this.clientAccountRepository = clientAccountRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void deleteByClientId(Integer id) {
        User user = userRepository.findByClientId(id);
        log.info("Deleting user " + user);
        userRepository.delete(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        final Role userRole = roleRepository.findByName(RoleType.USER.getRoleName());
        user.setRoles(Arrays.asList(userRole));

        Client client = user.getClient();
        final ClientAccount clientAccount = clientAccountFactory.createClientAccount(client);

        client.getAccounts().add(clientAccount);

        clientRepository.save(client);
        clientAccountRepository.save(clientAccount);
        userRepository.save(user);
    }

    @Override
    public void saveAdmin(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        final Role adminRole = roleRepository.findByName(RoleType.ADMIN.getRoleName());
        user.setRoles(Arrays.asList(adminRole));
        userRepository.save(user);
    }
}
