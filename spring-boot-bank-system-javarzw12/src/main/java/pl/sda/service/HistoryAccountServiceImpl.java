package pl.sda.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.ClientAccount;
import pl.sda.model.HistoryAccount;
import pl.sda.repository.HistoryAccountRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Transactional
@Service
public class HistoryAccountServiceImpl implements HistoryAccountService {

    private final HistoryAccountRepository accountRepository;

    public HistoryAccountServiceImpl(HistoryAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void save(String name, String surname, ClientAccount account, BigDecimal amount, String number) {

        HistoryAccount historyAccount = new HistoryAccount();
        historyAccount.setClientAccount(account);
        historyAccount.setAmount(amount);
        historyAccount.setName(name);
        historyAccount.setSurname(surname);
        historyAccount.setTransferDate(LocalDateTime.now());
        historyAccount.setNumber(number);

        accountRepository.save(historyAccount);

    }
}
