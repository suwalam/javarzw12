package pl.sda.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.configuration.PersonConfiguration;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;

public class AnnotationConfigurationMain {

    public static void main(String[] args) {

        ApplicationContext ac =
                new AnnotationConfigApplicationContext(PersonConfiguration.class);

        PersonService ps = ac.getBean("personServiceImpl", PersonServiceImpl.class);

        ps.getAll().forEach(System.out::println);

    }

}
