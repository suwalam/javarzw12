package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.model.Client;
import pl.sda.model.User;
import pl.sda.service.AutoLoginService;
import pl.sda.service.UserService;

@Slf4j
@Controller
public class RegistrationController {

    private final UserService userService;

    private final AutoLoginService autoLoginService;

    public RegistrationController(UserService userService, AutoLoginService autoLoginService) {
        this.userService = userService;
        this.autoLoginService = autoLoginService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        User user = new User();
        user.setClient(new Client());
        model.addAttribute("user", user);

        return "registration";
    }

    @PostMapping("/registration")
    public String handleRegistration(@ModelAttribute User user) {

        log.info("Received user to register: " + user);

        if (userService.findByUsername(user.getUsername()) != null) {
            log.info("User exists with username: " + user.getUsername());
            return "registration";
        }

        userService.saveUser(user);

        log.info("Registered user with username: " + user.getUsername());

        autoLoginService.autoLogin(user.getUsername(), user.getPassword());

        return "redirect:/client/details";
    }
}
