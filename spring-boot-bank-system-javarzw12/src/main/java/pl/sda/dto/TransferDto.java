package pl.sda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferDto {

    private String name;

    private String surname;

    private Integer accountId;

    private BigDecimal amount;

    private String number;
}
