package pl.sda.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Client;
import pl.sda.model.Role;
import pl.sda.model.RoleType;
import pl.sda.model.User;

import java.util.Arrays;

@SpringBootTest
class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Transactional
    @Test
    public void shouldSaveUser() {
        //given
        final Client testClient = new Client();
        testClient.setName("Michał");
        testClient.setSurname("Nowak");
        testClient.setAddress("Kraków");
        testClient.setPesel("1234567812");
        final User testUser = new User();
        testUser.setClient(testClient);
        testUser.setUsername("michal.nowak");
        final String rawPassword = "12345";
        testUser.setPassword(rawPassword);
        final Role userRole = roleService.findByName(RoleType.USER.getRoleName());
        testUser.setRoles(Arrays.asList(userRole));

        //when
        userService.saveUser(testUser);

        //then
        final User userFromDb = userService.findByUsername(testUser.getUsername());
        Assertions.assertEquals(testUser.getUsername(), userFromDb.getUsername());
        Assertions.assertTrue(bCryptPasswordEncoder.matches(rawPassword, userFromDb.getPassword()));
    }

}